# Static Web Site

This site is build using `Ruby Middleman`.

## Develop
Run `bundle install` first before running the `middleman server`.

Open the local site at [http://localhost:4567](http://localhost:4567)

## Build Project
Run `middleman build` to build the project, this will create build folder on the root directory.