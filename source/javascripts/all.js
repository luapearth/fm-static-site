//= require 'jquery'
//= require 'imagesloaded'
//= require 'skrollr'
//= require 'jquery.waypoints.min'
//= require 'zmain'

var running = circleAnimating = featuredAnimating = 0;
var arpos1 = 0;
var cycleTimeline = new TimelineMax({
    repeat: -1
});
var arrowpoint = new TimelineMax({
	repeat: -1
});

var tl = new TimelineMax();

function animateProcess() {
	if ( arpos1 == 0 ) {
		cycleTimeline.to(['#arc', '#arc_1_', '#arc_2_'], 10, {
		    rotation: 360,
		    transformOrigin: '50% 50%',
		    ease: Linear.easeNone
		});

		arpos1 = 1;
	}
}

function animateCollaboration() {
	tl.set('#fm', {yPercent: -250});
	tl.set('#users', {xPercent: 250});
	tl.set('#client', {xPercent: -250});


	tl.to(['#fm'], 0.7, {yPercent: 0, ease:Bounce.easeOut});
	tl.to(['#users', '#client'], 0.7, {xPercent: 0, ease:Bounce.easeOut});
	tl.to(['#userstext'], 0.7, {xPercent: 100, ease:Bounce.easeOut});
	circleAnimating = 1;

}

$('#slide-2 div').waypoint({
	handler: function(direction) {
		animateProcess();
	}
});

$('span[data-target-el="#slide-3"]').waypoint({
	handler: function(direction) {
		animateCollaboration();
	}
});

$('span[data-target-el="#slide-4"]').waypoint({
	handler: function(direction) {
		TweenMax.staggerFrom(".item", 0.5, {opacity: 0, rotation:360, y:200, scale: 2, delay:0.5}, 0.1);
		this.destroy();
	}
});